/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

namespace Trk {

inline bool
Track::isValid() const
{
  return (m_fitQuality && m_trackStateVector);
}

inline const FitQuality*
Track::fitQuality() const
{
  return m_fitQuality;
}

inline const DataVector<const TrackStateOnSurface>*
Track::trackStateOnSurfaces() const
{
  return m_trackStateVector;
}

inline DataVector<const TrackStateOnSurface>*
Track::trackStateOnSurfaces()
{
  return m_trackStateVector;
}

inline const TrackInfo&
Track::info() const
{
  return m_trackInfo;
}

inline TrackInfo&
Track::info()
{
  return m_trackInfo;
}

inline const Trk::TrackSummary*
Track::trackSummary() const
{
  return m_trackSummary.get();
}

inline Trk::TrackSummary*
Track::trackSummary()
{
  return m_trackSummary.get();
}

}
